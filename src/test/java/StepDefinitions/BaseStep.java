package StepDefinitions;

import Screens.*;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BaseStep {
    static WebDriver driver;

    HalUtamaEvermos halUtamaEvermos = new HalUtamaEvermos(driver);
    LoginPage loginPage = new LoginPage(driver);
    CatalogPage catalogPage = new CatalogPage(driver);
    SearchPage searchPage = new SearchPage(driver);
    RekomendasiPage rekomendasiPage = new RekomendasiPage(driver);
    DetailProdukPage detailProdukPage = new DetailProdukPage(driver);
    AlamatPengirimanPage alamatPengirimanPage = new AlamatPengirimanPage(driver);
    KeranjangPage keranjangPage = new KeranjangPage(driver);

}
