package StepDefinitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Hook {

    @After
    public void closeBrowser(){
        BaseStep.driver.quit();
    }

    @Before
    public void initBrowser(){
        System.setProperty("webdriver.chrome.driver", "webdriver/chromedriver");
        BaseStep.driver = new ChromeDriver();
        BaseStep.driver.get("https://evermos.com/");
        BaseStep.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

}
