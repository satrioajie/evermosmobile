package StepDefinitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Search extends BaseStep {

    @When("^user mengklik search bar di halaman katalog$")
    public void user_mengklik_search_bar_di_halaman_katalog() throws Throwable {
        catalogPage.klikSearchBar();
    }

    @When("^user mengisi keyword \"([^\"]*)\" di halaman pencarian$")
    public void user_mengisi_keyword_di_halaman_pencarian(String keyword) throws Throwable {
        searchPage.fillSearchBarText(keyword);
    }

    @When("^user mengklik hasil pencarian \"([^\"]*)\" di halaman pencarian$")
    public void user_mengklik_hasil_pencarian_di_halaman_pencarian(String keyword) throws Throwable {
        searchPage.clickSearchResult(keyword);
    }

    @Then("^halaman brand \"([^\"]*)\" muncul di halaman rekomendasi$")
    public void halaman_brand_muncul_di_halaman_rekomendasi(String brandName) throws Throwable {
        rekomendasiPage.validateBrandName(brandName);
    }

    @When("^user mengklik tombol BATAL di halaman pencarian$")
    public void user_mengklik_tombol_BATAL_di_halaman_pencarian() throws Throwable {
        searchPage.clickBatalButton();
    }


}
