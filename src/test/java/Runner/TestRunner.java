package Runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/",
        tags = "~@dev",
        glue = "StepDefinitions",
        plugin = {
                "pretty",
                "html:target/site/cucumber-pretty01",
                "json:target/report.json"
        }
)

public class TestRunner {

}
