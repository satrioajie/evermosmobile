package Screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AlamatPengirimanPage extends BaseScreen {
    @FindBy(xpath = "//input[@class='searchAddress__input']")
    WebElement searchText;

    @FindBy(xpath = "//button[@class='searchAddress__button']")
    WebElement searchButton;

    @FindBy(xpath = "//button[.='Pilih Alamat']")
    WebElement pilihAlamatButton;

    public AlamatPengirimanPage(WebDriver driver) {
        super(driver);
    }

    public void searchKeywordAlamat(String keyword){
        searchText.sendKeys(keyword);
        searchButton.click();
    }

    public void selectAlamat(String alamat){
        driver.findElement(By.xpath("//span[.='"+alamat+"']/../input")).click();
    }

    public void clickPilihAlamat(){
        pilihAlamatButton.click();
    }
}
