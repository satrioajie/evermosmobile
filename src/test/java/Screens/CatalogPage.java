package Screens;

import org.junit.Assert;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CatalogPage extends BaseScreen {

    @FindBy(xpath = "//a[contains(.,'Jualan apa sekarang ?')]")
    WebElement searchBar;

    @FindBy(id="onesignal-slidedown-cancel-button")
    WebElement lainKaliButton;

    public CatalogPage(WebDriver driver) {
        super(driver);
    }

    public void validateTitleCatalogPage(){
        WebDriverWait wait =  new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.titleIs("Evermos - Katalog"));
        Assert.assertEquals("title tidak sesuai","Evermos - Katalog", driver.getTitle());

        try {
            wait.until(ExpectedConditions.visibilityOf(lainKaliButton)).click();
        }catch (TimeoutException e){

        }
    }

    public void klikSearchBar(){
        searchBar.click();
    }

}
