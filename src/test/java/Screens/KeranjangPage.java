package Screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class KeranjangPage extends BaseScreen {

    public KeranjangPage(WebDriver driver) {
        super(driver);
    }

    public void validateProductinKeranjang(String prodName){
        driver.findElement(By.xpath("//a[.='MQ Apparel - Jaket Rompi Air In L BLACK']")).isDisplayed();
    }

    public void validateAlamatTujuan(String alamat){
        driver.findElement(By.xpath("//b[.='"+alamat+"']")).isDisplayed();
    }
}
