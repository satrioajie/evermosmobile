package Screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DetailProdukPage extends BaseScreen {

    @FindBy(xpath = "//a[contains(.,'Order')]")
    WebElement orderButton;

    @FindBy(xpath = "//div[.='Produk berhasil ditambahkan ke keranjang']")
    WebElement successAddToCartNotifMsg;

    @FindBy(xpath = "//button[@class='btn btn--small btn--brand']")
    WebElement lihatKeranjangButton;

    public DetailProdukPage(WebDriver driver) {
        super(driver);
    }

    public void selectUkuran(String size){
        driver.findElement(By.xpath("//span[.='"+size+"']")).click();
    }

    public void selectWarna(String colour){
        driver.findElement(By.xpath("//span[.='"+colour+"']")).click();
    }

    public void clickOrderButton(){
        orderButton.click();
    }

    public void validateSuccessAddToCart(){
        successAddToCartNotifMsg.isDisplayed();
    }

    public void clickLihatKeranjangButton() throws InterruptedException {
        Thread.sleep(3000);
        lihatKeranjangButton.click();
    }
}
