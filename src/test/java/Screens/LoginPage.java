package Screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BaseScreen {

    @FindBy(css="input[placeholder='nomor telepon anda']")
    WebElement noTelpText;

    @FindBy(css = "input[placeholder='kata sandi anda']")
    WebElement passText;

    @FindBy(xpath = "//button[@class='btn btn--brand btn--block btn--large']")
    WebElement masukButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void fillNoTelpText(String noTelp){
        noTelpText.sendKeys(noTelp);
    }

    public void fillPassText(String pass){
        passText.sendKeys(pass);
    }

    public void klikMasukButton(){
        masukButton.click();
    }

    public void validateErrorNotifExist(String errorNotif){
        driver.findElement(By.xpath("//*[.='"+errorNotif+"']")).isDisplayed();
    }
}
