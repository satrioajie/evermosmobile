package Screens;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RekomendasiPage extends BaseScreen {
    @FindBy(xpath = "//div[@class='selectedBrand__name']")
    WebElement brandName;

    public RekomendasiPage(WebDriver driver) {
        super(driver);
    }

    public void validateBrandName(String bN){
        Assert.assertEquals("Nama brand salah",bN,brandName.getText());
    }

    public void clickProduct(String prodName){
        WebElement element = driver.findElement(By.xpath("//a[.='"+prodName+"']"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);

        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[.='"+prodName+"']"))).click();
//        driver.findElement(By.xpath("//a[.='"+prodName+"']")).click();
    }

}
