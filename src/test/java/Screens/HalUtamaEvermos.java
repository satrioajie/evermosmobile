package Screens;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HalUtamaEvermos extends BaseScreen {

    @FindBy(xpath = "//button[.='Masuk']")
    WebElement masukButton;

    public HalUtamaEvermos(WebDriver driver) {
        super(driver);
    }

    public void clickMasukButton(){
        masukButton.click();
    }

    public void validateHalUtamaEvermos(){
        Assert.assertEquals("halaman salah","Peluang Usaha Sampingan Reseller Indonesia Dropship Produk Muslim | Evermos",driver.getTitle());
    }
}
