Feature: Pembelian Produk


Scenario: pembelian dengan produk di salah satu brand
  Given user membuka halaman web evermoss
  And user mengklik tombol Masuk di halaman utama evermos
  And user mengisi nomor telepon "12233344445555" di halaman login
  And user mengisi password "password" di halaman login
  And user mengklik tombol Masuk di halaman login
  And halaman catalog akan muncul
  And user mengklik search bar di halaman katalog
  And user mengisi keyword "MQ Apparel" di halaman pencarian
  And user mengklik hasil pencarian "MQ Apparel" di halaman pencarian
  And halaman brand "MQ Apparel" muncul di halaman rekomendasi
  When user memilih produk "MQ Apparel - Jaket Rompi Air In" di halaman rekomendasi
  And user mengklik Order di halaman detail produk
  And user memasukkan keyword "Khadijah" di halaman alamat pengiriman
  And user memilih alamat "Rumah Khadijah" sebagai alamat tujuan pengiriman di halaman alamat pengiriman
  And user mengklik tombol Pilih Alamat di halaman alamat pengiriman
  Then notifikasi produk berhasil ditambahkan ke keranjang muncul
  And user mengklik tombol Lihat Keranjang pada notifikasi produk berhasil ditambahkan ke keranjang
  And produk "MQ Apparel - Jaket Rompi Air In L BLACK" tampil di halaman keranjang dengan alamat pengiriman "Rumah Khadijah"
