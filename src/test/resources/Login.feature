Feature: Login

Scenario: login dengan credential yang valid
  Given user membuka halaman web evermoss
  When user mengklik tombol Masuk di halaman utama evermos
  And user mengisi nomor telepon "12233344445555" di halaman login
  And user mengisi password "password" di halaman login
  And user mengklik tombol Masuk di halaman login
  Then halaman catalog akan muncul

Scenario: login tanpa mengisi nomer telpon dan password
  Given user membuka halaman web evermoss
  When user mengklik tombol Masuk di halaman utama evermos
  And user mengklik tombol Masuk di halaman login
  Then muncul notifikasi error "Nomor telepon harus di isi" di halaman login
  And muncul notifikasi error "Kata sandi harus di isi" di halaman login

Scenario: login dengan menggunakan password yang salah
  Given user membuka halaman web evermoss
  When user mengklik tombol Masuk di halaman utama evermos
  And user mengisi nomor telepon "12233344445555" di halaman login
  And user mengisi password "awdxawdx" di halaman login
  And user mengklik tombol Masuk di halaman login
  Then muncul notifikasi error "Nomor Telepon atau Kata Sandi anda salah!" di halaman login

  Scenario: login dengan menggunakan nomer telpon yang tidak terdaftar
    Given user membuka halaman web evermoss
    When user mengklik tombol Masuk di halaman utama evermos
    And user mengisi nomor telepon "1223334444555566" di halaman login
    And user mengisi password "awdxawdx" di halaman login
    And user mengklik tombol Masuk di halaman login
    Then muncul notifikasi error "Nomor ini belum terdaftar sebagai reseller" di halaman login